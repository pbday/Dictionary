import dictionary._
import org.scalatest.{BeforeAndAfterEach, FlatSpec}

/**
  * This is the test file for the DictServer object which runs a remote dictionary server. This can be
  * found within src/main/scala/dictionary/dictionary_server. That file also details the dependencies and other
  * running instructions and so should be referred to before running the module.
  *
  * This tests test the dictserver methods function as specified. That they run successfully from different processes
  * and different machines was checked manually as the test code was unable to spawn different processes. It was also
  * checked manually that all processes terminated after running, again due to this not being able to be observed within
  * this test file.
  *
  * This doesn't test what is printed on screen, only what the methods return, as the specification didn't specify
  * what needed to be logged to screen.
  *
  * Functions that return notfound (remove and lookup) return error if the dictionary server is not running. The spec
  * did not specify what to do in this instance and returning error made more sense.
  *
  */

class DictServerSpec extends FlatSpec with BeforeAndAfterEach {

  override def beforeEach(){DictServer.stop}

  "The start function" should "return an error if it has already been started" in {
    assert(DictServer.start == ok)
    assert(DictServer.start == error)
  }
  it should "start again if it is run after a previous version has been stopped" in {
    assert(DictServer.start == ok)
    DictServer.stop
    assert(DictServer.start == ok)
  }

  "The stop function " should "not cause an error if the dictionary server does not exist" in {
    DictServer.stop
    assert(true)
  }
  it should "stop the dictionary process from running" in {
    assert(DictServer.start == ok)
    assert(DictServer.start == error)
    DictServer.stop
    assert(DictServer.start == ok)
  }

  "The insert function" should "return an error if the dictionary does not exist" in {
    assert(DictServer.insert(1,2)==error)
  }
  it should "add the value to the dictionary if it doesn't already exist" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.lookup(1).asInstanceOf[(Response,Int)]._2 == 2)
  }
  it should "overwrite the value in the dictionary if it doesn't already exist" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert(1,3)
    assert(DictServer.lookup(1).asInstanceOf[(Response,Int)]._2 == 3)
  }
  it should "return ok when inserting a value into the dictionary" in {
    DictServer.start
    assert(DictServer.insert(1,2)==ok)
  }
  it should "return ok when overwriting a value in a dictionary" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.insert(1,4)==ok)
  }
  it should "work with varied types and multiple entries" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert("word",2.8f)
    assert(DictServer.lookup(1).asInstanceOf[(Response,Int)]._2 == 2)
    assert(DictServer.lookup("word").asInstanceOf[(Response,Float)]._2 == 2.8f)
  }

  "The remove function" should "return an error if the dictionary does not exist" in {
    assert(DictServer.remove(1)==error)
  }
  it should "return notfound if it is not contained within the dictionary" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.remove(3)==notfound)
  }
  it should "return notfound when the dictionary is empty" in {
    DictServer.start
    assert(DictServer.remove(1)==notfound)
  }
  it should "remove a value from the dictionary" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.remove(1)
    assert(DictServer.lookup(1)==notfound)
  }
  it should "return ok when removing a value from a dictionary" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.remove(1)==ok)
  }
  it should "return notfound once its been removed before" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.remove(1)
    assert(DictServer.remove(1)==notfound)
  }
  it should "remove a variety of different types" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert("word",2.8f)
    DictServer.remove(1)
    DictServer.remove("word")
    assert(DictServer.lookup(1) == notfound)
    assert(DictServer.lookup("word") == notfound)
  }
  it should "not find a value present previously when the server is started then stopped" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.stop
    DictServer.start
    assert(DictServer.remove(1)==notfound)
  }

  "The lookup function" should "return an error if the dictionary does not exist" in {
    assert(DictServer.lookup(1)==error)
  }
  it should "return notfound if the value was not present in the dictionary" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.lookup(2)==notfound)
  }
  it should "return notfound if searching an empty dictionary" in {
    DictServer.start
    assert(DictServer.lookup(1)==notfound)
  }
  it should "not remove the value from a dictionary when found" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.lookup(1).asInstanceOf[(Response,Int)]._2==2)
    assert(DictServer.lookup(1).asInstanceOf[(Response,Int)]._2==2)
  }
  it should "return the tuple (ok,val) when the value is found" in {
    DictServer.start
    DictServer.insert(1,2)
    val (resp,value) = DictServer.lookup(1).asInstanceOf[(Response,Int)]
    assert(resp==ok)
    assert(value==2)
  }
  it should "work for a variety of different types and multiple entries" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert("word",2.6f)
    val (r1,v1) = DictServer.lookup(1).asInstanceOf[(Response,Int)]
    val (r2,v2) = DictServer.lookup("word").asInstanceOf[(Response,Float)]
    assert(r1==ok)
    assert(r2==ok)
    assert(v1==2)
    assert(v2==2.6f)

  }
  it should "not find a value that has been removed" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.remove(1)
    assert(DictServer.lookup(1)==notfound)
  }
  it should "not find a value that was in the dictionary previously after a restart of the dictionary server" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.stop
    DictServer.start
    assert(DictServer.lookup(1)==notfound)
  }

  "The clear function" should " not throw any exception if the distionary does not exist" in {
    DictServer.clear
    assert(true)
  }
  it should "not throw an exceptions if it is run on an empty dictionary" in {
    DictServer.start
    DictServer.clear
    assert(true)
  }
  it should "remove all entries from a dictionary" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert(2,3)
    DictServer.clear
    assert(DictServer.size==0)
  }
  it should "mean values will not be found after which were present before" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.clear
    assert(DictServer.lookup(1)==notfound)
  }

  "The size function" should "return 0 if the dictionary does not exist" in {
    assert(DictServer.size==0)
  }
  it should "be zero when restarting a dictionary having closed it down" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.stop
    DictServer.start
    assert(DictServer.size==0)
  }
  it should "increase by 1 when a new value is added" in {
    DictServer.start
    DictServer.insert(1,2)
    assert(DictServer.size==1)
  }
  it should "not increase in size when a value is overwritten" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert(1,3)
    assert(DictServer.size==1)
  }
  it should "it should be zero when a new server is started" in {
    DictServer.start
    assert(DictServer.size == 0)
  }
  it should "count for a variety of types" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert("word",3.6f)
    assert(DictServer.size==2)
  }
  it should "reduce in count when an item is removed" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert(3,4)
    DictServer.remove(1)
    assert(DictServer.size==1)
  }
  it should "not reduce in count when an item is lookedup" in {
    DictServer.start
    DictServer.insert(1,2)
    DictServer.insert(3,4)
    DictServer.lookup(1)
    assert(DictServer.size==2)
  }








}
