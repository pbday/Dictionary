package dictionary


import java.util.concurrent.{TimeoutException, TimeUnit}
import akka.actor._
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.jboss.netty.channel.ChannelException
import scala.concurrent.{Future, Await}
import akka.pattern.ask

/**
  * This DictServer object contains the methods for coursework 4 for the PPL module.
  * It has been coded using akka in scala, to the SBT file layout.
  *
  * The dependencies file build.sbt contains all the library dependencies for running this code.
  *
  * DictServer creates a server which can then be accessed either from the same machine or remotely. To run this program
  * it is required that the server IP address and local IP address are stored within the srs/main/resources/application.conf file.
  *
  * Settings common to both Local and Server, such as which logs to display, can be set in the src/main/resources/common.conf file.
  *
  * The corresponding test file can be found in src/test/scala/DictServerSpec
  *
  * The program has one setting within the code, TIME_TO_WAIT, which the user can use to determine how long a call to
  * the server should wait until it determines it cannot be found. Once this time has elapsed, it makes no further effort
  * to make contact (such as checking again after a longer time period).
  *
  * The program contains the following methods, as outlined in the spec:
  *   - start - starts the server
  *   - stop - stops the server
  *   - insert - inserts a value into the dictionary
  *   - remove - removes a value from the dictionary
  *   - lookup - returns a value from the dictionary
  *   - clear - clears the dictionary of all stored keys and values
  *   - size - returns the amount of key-value pairs stored within the dictionary
  *
  * The following should also be noted:
  *   - If the server doesn't exist, it will wait for the full timeout time before returning that it doesnt exist.
  *   - For methods that return notfound (lookup and remove), "error" rather than "notfound" is returned if the server
  *     isn't running, as tghe spec didn't mention this specific case and it made more sense.
  *
  */

object DictServer {

  /* The length of time to wait before any request times out */
 def TIME_TO_WAIT : Timeout = new Timeout(1,TimeUnit.SECONDS)

  /**
    * Starts the dictionary server on the on the server system. Returns ok if it worked or error should
    * it already exist
    * @return Whether the dictionary was set up properly, ok if so, error if not
    */
  def start : Response = {
    try {
      val system = ActorSystem("Server", ConfigFactory.load.getConfig("Server"))
      system.actorOf(Props[ServerActor], name = "ServerActor")
      ok
    } catch {
      case e: ChannelException =>
        println("Dictionary already exists (ChannelException thrown)")
        error
    }
  }

  /**
    * Stops the dictionary server running.
    */
  def stop = sendMessage[Any](Stop)

  /**
    * Inserts a value into the dictionary for the given key. In will overwrite the value if the key already exists.
    * Returns either ok if it is added or error if the dictionary server couldn't be found.
    *
    * @param key The key value in the dictionary
    * @param value The value to be added for the key
    * @return ok if the value is inserted correctly, or error if it isn't
    */
  def insert(key:Any,value:Any) : Response = sendMessage[Response](Insert(key,value)).getOrElse(error)

  /**
    * Removes a value from the dictionary. This returns ok if it was found and removed, or notfound if it was not found in the
    * dictionary, or error if the dictionary server could not be found.
    * @param key The value to find in the dictionary
    * @return A response, either ok(ir removed), notfound (if not in dictionary) or error (if server not found)
    */
  def remove(key:Any) : Response = sendMessage[Response](Remove(key)).getOrElse(error)

  /**
    * Lookups up the associated value for a key. Returns the tuple (ok,value) if it is found, notfound if it isn't, and error if the dictionary server does not exist
    * @param key The value to lookup
    * @return Either the tuple contianing the value, notfound or error
    */
  def lookup(key:Any) : Any = sendMessage[Any](Lookup(key)).getOrElse(error)

  /**
    * Clears all keys and values out of the dictionary. Doesn't return anything, even if the dictionary doesn't exist.
    * @return
    */
  def clear = sendMessage[Any](Clear)

  /**
    * Returns the size of the dictioanry. If it can't be found, it returns 0.
    * @return The size of the dictionary
    */
  def size : Int = sendMessage[Int](Size).getOrElse(0)

  // This is the function used to send messages to the dictionary server.
  private def sendMessage[T](message: Message) :Option[T] = {
    val locSystem = ActorSystem("LocalSystem", ConfigFactory.load.getConfig("Local"))
    val act = locSystem.actorSelection(s"akka.tcp://Server@$serverIP:$serverPort/user/ServerActor")
    implicit val timeout = TIME_TO_WAIT//Timeout(5,TimeUnit.SECONDS)

    if (act.resolveOne() == null) println("actor is null!")

    try{
    val future :Future[Any] = act ? message
    val rtn = Await.result(future,timeout.duration).asInstanceOf[T]
    Some(rtn)
    } catch {
      case e: TimeoutException =>
            println("Dictionary doesn't exist")
            None
    } finally {
      locSystem.terminate()
    }
  }

  private def serverIP : String = ConfigFactory.load.getConfig("Server").getString("akka.remote.netty.tcp.hostname")
  private def serverPort : Int = ConfigFactory.load.getConfig("Server").getInt("akka.remote.netty.tcp.port")

}

//This class is the dictionary server actor which stores the dictionary and receives messages
class ServerActor extends Actor {

  var dictMap = scala.collection.mutable.Map[Any,Any]()

  override def receive: Receive = {
    case Stop => {
      sender ! ok
      context.system.terminate()
    }
    case Insert(a,b) =>
      dictMap.put(a,b)
      sender ! ok
    case Lookup(a) =>
        dictMap.get(a) match {
      case None => sender ! notfound
      case Some(n) => sender ! (ok,n)
    }
    case Clear =>
      dictMap.clear()
      sender ! ok
    case Size => sender ! dictMap.size
    case Remove(a) => if (!dictMap.contains(a)) sender ! notfound
                        else
                          dictMap.remove(a)
                          sender ! ok
  }
}


