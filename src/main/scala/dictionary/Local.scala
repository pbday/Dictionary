package dictionary


object Local extends App {

//  implicit val system = ActorSystem("LocalSystem", ConfigFactory.load.getConfig("Local"))
//  val localActor = system.actorOf(Props[LocalActor], name = "LocalActor")                                                     // start the action

  DictServer.start

}

object StopRun extends App {
  DictServer.stop
}

object InsertFunc extends App{
  println(DictServer.insert(1,3))
}

object LookupFun extends App{
  println(DictServer.lookup(1))
}

object RemoveFun extends App{
  println(DictServer.remove(1))
}

object GetSize extends App {
  println(DictServer.size)
}

object ClearDict extends App{
  println(DictServer.clear)
}

object LookupAndIns extends App{
  DictServer.insert(1,2)
  println(  println(DictServer.lookup(1).asInstanceOf[(Response,Int)]._2))
}
