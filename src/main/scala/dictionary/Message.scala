package dictionary

/**
  * This file contains the possible messages to be sent to the Dictionary server as well as the
  * possible responses
  */

sealed trait Message
case object Start extends Message
case object Stop extends Message
case class Insert(a:Any,b:Any) extends Message
case class Remove(a:Any) extends Message
case class Lookup(a:Any) extends Message
case object Clear extends Message
case object Size extends Message

sealed trait Response
case object ok extends Response
case object notfound extends Response
case object error extends Response