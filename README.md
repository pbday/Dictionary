This is the Dictionary Server program as specified in coursework 4 for the PPL Module. It has been coded using Akka in Scala, to the SBT file layout.


The dependencies file build.sbt contains all the library dependencies for running this code.


It contains the following key files:

* Dependencies file: build.sbt

* The main program: src/main/scala/dictionary/dictionary_server.scala

* The test file: src/test/scala/DictServerSpec

* Settings files required for set up before use: src/main/resources/application.conf and src/main/resources/common.conf


There are two other files within the same package as the main program:

* Message.scala: Contins traits used for messages and for output

* Local.scala: Contains short App methods used to run a method on DictServer. This is not required to use the main program, they were used to test methods run remotely.


The main program contains an object called DictServer, which contains the methods specified. This creates a server which can then be accessed either from the same machine or remotely. The main program file also contains important running notes, including the following:

* To run this program it is required that the server IP address and local IP address are stored within the application.conf settings file.

* Settings common to both Local and Server, such as which logs to display, can be set in the common.conf file.

* The program has one setting within the code, TIME_TO_WAIT, which the user can use to determine how long a call to the server should wait until it determines it cannot be found. Once this time has elapsed, it makes no further effort to make contact (such as checking again after a longer time period).


The main program contains the following methods, as outlined in the spec:

* start - starts the server

* stop - stops the server

* insert - inserts a value into the dictionary

* remove - removes a value from the dictionary

* lookup - returns a value from the dictionary

* clear - clears the dictionary of all stored keys and values

* size - returns the amount of key-value pairs stored within the dictionary


The following should also be noted:

* If the server doesn't exist, it will wait for the full timeout time before returning that it doesnt exist.

* For methods that return notfound (lookup and remove), "error" rather than "notfound" is returned if the server isn't running, as tghe spec didn't mention this specific case and it made more sense.